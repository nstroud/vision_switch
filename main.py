import mark1.vision_switch as executor
import environment
import light_source

def main():
    env_source = environment.get_environment()
    executor.execute(
        env_source, 
        light_source.get_light_source(env_source)
        )  

if __name__=="__main__":
    main()