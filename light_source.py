from enum import Enum
from environment import Environment

class LightSource(Enum):
    NANO_LEAF = 1
    IN_VIDEO_FRAME = 2
    LED = 3

def get_light_source(environment: Environment):
    if environment == Environment.COMPUTER:
        return LightSource.IN_VIDEO_FRAME
    else:
        return LightSource.LED