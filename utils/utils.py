import cv2

def draw_box(image, bbox, color=(255, 0, 255), fill=False):
    x, y, width, height = [int(x) for x in bbox]
    thickness = -1 if fill else 3
    cv2.rectangle(image, (x, y), (x + width, y + height), color, thickness, 1)


def contained(point, rect):
    """is point in box"""
    x1, y1, w, h = rect
    x2, y2 = x1+w, y1+h
    x, y = point
    if x1 < x < x2:
        if y1 < y < y2:
            return True
    return False


def intersection(a, b):
    """a and b given as point and w h"""
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0]+a[2], b[0]+b[2]) - x
    h = min(a[1]+a[3], b[1]+b[3]) - y
    if w < 0 or h < 0:
        return None
    return x, y, w, h


def get_bounding_box(hand_landmarks, image):
    bounding_box = [1, 1, 0, 0]
    for l in mp_hands.HandLandmark:
        landmark = hand_landmarks.landmark[l]
        if landmark.x < bounding_box[0]:
            bounding_box[0] = landmark.x
        if landmark.y < bounding_box[1]:
            bounding_box[1] = landmark.y
        if landmark.x > bounding_box[2]:
            bounding_box[2] = landmark.x
        if landmark.y > bounding_box[3]:
            bounding_box[3] = landmark.y
    image_height, image_width, _ = image.shape
    bounding_box[0] = int(bounding_box[0] * image_width)
    bounding_box[1] = int(bounding_box[1] * image_height)
    bounding_box[2] = int(bounding_box[2] * image_width)
    bounding_box[3] = int(bounding_box[3] * image_height)
    return bounding_box


def convert_bounding_box(box):
    return box[0], box[1], box[2] - box[0], box[3]-box[1]


def extract_x_y(landmark):
    return landmark.x, landmark.y
