import math


def dist(p, q):
    return math.sqrt(sum((px - qx) ** 2.0 for px, qx in zip(p, q)))


def angle_from_points(p1, p2, p3):
    return angle(dist(p1, p2), dist(p1, p3), dist(p2, p3))


def angle(a, b, c):
    return math.acos((a**2 + b**2 - c**2)/(2*a*b))
