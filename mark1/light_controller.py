import mediapipe as mp
from utils.utils import draw_box, contained, extract_x_y
from utils.math_helper import angle_from_points
import math
mp_pose = mp.solutions.pose


class LightController:

    MIN_ANGLE_DIFF = 25
    MIN_ANGLE_RANGE = 115
    MAX_ANGLE_RANGE = 180
    in_control_range = False
    shoulder_angle = 0

    def __init__(self, switch):
        self.switch = switch
        self.last_frame_overlap = False
        self.control_angle = 0
        
        self._last_frame_wrist_bbox = None

    def process_frame(self, pose_landmarks):
        if pose_landmarks:
            self._process_switch(pose_landmarks)
            self._process_brightness(pose_landmarks)
        else:
            self.switch.turn_off()

    def _process_brightness(self, pose_landmarks):
        left_shoulder = extract_x_y(
            pose_landmarks.landmark[mp_pose.PoseLandmark.LEFT_SHOULDER])
        right_shoulder = extract_x_y(
            pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_SHOULDER])
        wrist = extract_x_y(
            pose_landmarks.landmark[mp_pose.PoseLandmark.RIGHT_WRIST])
        shoulder_angle = angle_from_points(
            right_shoulder, wrist, left_shoulder)
        shoulder_angle = math.degrees(shoulder_angle)
        self.control_angle = shoulder_angle

        in_control_range = LightController.is_in_control_range(shoulder_angle) and wrist[1] >= right_shoulder[1]
        if in_control_range:
            brightness = LightController.get_brightness(shoulder_angle)
            self.switch.set_brightness(brightness, 0)

    def _process_switch(self, pose_landmarks):
        if pose_landmarks:
            self.switch.turn_on()

    @staticmethod
    def is_in_control_range(angle_in_degrees):
        return angle_in_degrees > 90 + LightController.MIN_ANGLE_DIFF or \
               angle_in_degrees < 90 - LightController.MIN_ANGLE_DIFF

    @staticmethod
    def get_brightness(shoulder_angle):
        if shoulder_angle < 90:
            shoulder_angle = 180 - shoulder_angle
        return int(100 * (shoulder_angle - LightController.MIN_ANGLE_RANGE) /
                   (LightController.MAX_ANGLE_RANGE-LightController.MIN_ANGLE_RANGE))
