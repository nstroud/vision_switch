from mark1.switch_base import Switch
import RPi.GPIO as GPIO

class LedSwitch(Switch):
    RED_PIN = 19
    BLUE_PIN = 20
    GREEN_PIN = 18

    def __init__(self):
        super(LedSwitch, self).__init__()
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(LedSwitch.RED_PIN, GPIO.OUT)
        GPIO.setup(LedSwitch.BLUE_PIN, GPIO.OUT)
        GPIO.setup(LedSwitch.GREEN_PIN, GPIO.OUT)
        GPIO.setwarnings(True)
        
        self.red_pwm = GPIO.PWM(LedSwitch.RED_PIN, 100)
        self.blue_pwm = GPIO.PWM(LedSwitch.BLUE_PIN, 100)
        self.green_pwm = GPIO.PWM(LedSwitch.GREEN_PIN, 100)

        # list the colors that should be used
        self.pwms = (self.red_pwm,)

        for pwm in self.pwms:
            pwm.start(100)

    def turn_on(self):
        self.on = True
        self._update()

    def turn_off(self):
        self.on = False
        self._update()

    def toggle(self):
        self.on = not self.on
        self._update()

    def set_brightness(self, brightness: int, duration: int = 10):
        super().set_brightness(brightness, duration)
        self._update()

    def _update(self):
        if self.on:
            duty_cycle = self.brightness
        else:
            duty_cycle = 0

        # for pwm in (self.red_pwm, self.blue_pwm, self.green_pwm):
        for pwm in self.pwms:
            pwm.ChangeDutyCycle(duty_cycle)


# import RPi.GPIO as GPIO
# import time
# import math

# red_pin = 19
# blue_pin = 20
# green_pin = 18
# GPIO.setwarnings(False)
# GPIO.setmode(GPIO.BCM)
# GPIO.setup(red_pin, GPIO.OUT)
# GPIO.setup(blue_pin, GPIO.OUT)
# GPIO.setup(green_pin, GPIO.OUT)

# power_level = 50
# red_pwm = GPIO.PWM(red_pin, 100)
# blue_pwm = GPIO.PWM(blue_pin, 100)
# green_pwm = GPIO.PWM(green_pin, 100)
# red_pwm.start(power_level)
# blue_pwm.start(power_level)
# green_pwm.start(power_level)

# pi = 3.14
# def pulse(time, frequency):
#     """
#     Frequency in Hz
#     """
#     return (0.5 * (1 + math.sin(2 * pi * frequency * time))) * 100

# while True:
#     red_pwm.ChangeDutyCycle(pulse(time.time(), .1))
#     blue_pwm.ChangeDutyCycle(pulse(time.time(), .2))
#     green_pwm.ChangeDutyCycle(pulse(time.time(), .3))