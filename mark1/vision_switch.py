import math
import cv2
import time
import mediapipe as mp

from environment import Environment
from light_source import LightSource
from mark1.light_controller import LightController
from utils.utils import draw_box, extract_x_y, contained
from utils.math_helper import angle_from_points

mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_pose = mp.solutions.pose

WINDOW_NAME = "Tracking"
FIXED_SWITCH_LOCATION = True
VIDEO_CAPTURE_CHANNEL = 0
DEBUG = True

def execute(environment: Environment, light_source: LightSource):

    if light_source == LightSource.NANO_LEAF:
        from switch_nanoleaf import Switch
    elif light_source == LightSource.IN_VIDEO_FRAME:
        from mark1.switch_computer import Switch
    elif light_source == LightSource.LED:
        from mark1.switch_led import LedSwitch as Switch

    cam = cv2.VideoCapture(VIDEO_CAPTURE_CHANNEL)
    switch = Switch()
    controller = LightController(switch)

    # sometimes camera is not ready
    time.sleep(1)

    _, image = cam.read()
    image_height, image_width, _ = image.shape
    image_mid_y, image_mid_x = [int(x / 2) for x in (image_height, image_width)]

    timer = cv2.getTickCount()
    counter = 0
    fps = 0
    last_frame_overlap = False

    # shoulder_angle = 0
    # in_control_range = False

    with mp_pose.Pose(
            min_detection_confidence=0.5,
            min_tracking_confidence=0.5) as pose:
        while cam.isOpened():
            _, image = cam.read()
            # Flip the image horizontally for a later selfie-view display, and convert
            # the BGR image to RGB.
            image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
            image.flags.writeable = False
            results = pose.process(image)
            image.flags.writeable = True

            controller.process_frame(results.pose_landmarks)
            
            if DEBUG:
                if not counter % 7:
                    fps = cv2.getTickFrequency()/(cv2.getTickCount()-timer)
                timer = cv2.getTickCount()


            if light_source == LightSource.IN_VIDEO_FRAME:
                image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

                # if not results.pose_landmarks:
                #     cv2.putText(image, 'lost', (75, 75),
                #                 cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

                # draw FPS
                cv2.putText(image, str(int(fps)), (75, 50),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

                # draw shoulder angle
                # cv2.putText(image, str(controller.control_angle), (75, 70),
                #             cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

                # draw switch
                # draw_box(image, switch.bbox)

                # draw light output
                light_text_location = (10, image_height - 240)
                if switch.is_on():
                    cv2.putText(image, "ON", light_text_location,
                                cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 80, 255), 2)
                else:
                    cv2.putText(image, "OFF", light_text_location,
                                cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 80, 255), 2)

                if light_source is not LightSource.NANO_LEAF:
                    light_box = (10, image_height - 210, 300, 200)
                    draw_box(image, light_box, color=switch.get_rgb_color(), fill=True)

                # draw pose
                if results.pose_landmarks:
                    mp_drawing.draw_landmarks(
                        image,
                        results.pose_landmarks,
                        mp_pose.POSE_CONNECTIONS,
                        landmark_drawing_spec=mp_drawing_styles.get_default_pose_landmarks_style())

            if environment == Environment.COMPUTER:
                cv2.imshow(WINDOW_NAME, image)
            
            print(f'fps:{fps} on:{switch.on} brightness: {switch.brightness}')

            if cv2.waitKey(1) & 0xff == ord('q'):
                break
            counter += 1
