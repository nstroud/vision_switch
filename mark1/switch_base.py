import os

class Switch:

    def __init__(self):
        self.on = False
        self.brightness = 100
        self.bbox = 0, 0, 10, 10

    def is_on(self):
        return self.on

    def turn_on(self):
        self.on = True

    def turn_off(self):
        self.on = False

    def toggle(self):
        self.on = not self.on

    def play_sound(self):
        if self.on:
            os.system('say "On"')
        else:
            os.system('say "off"')

    def set_brightness(self, brightness: int, duration: int = 10):
        if self.is_on():
            self.brightness = brightness

    def get_rgb_color(self):
        if not self.on:
            return 0, 0, 0
        else:
            color = int(max(self.brightness, 20) / 100 * 255)
            return color, color, color
