import os

from enum import Enum 

class Environment(Enum):
    RASPBERRY_PI = 1
    COMPUTER = 2

def get_environment():
    user = os.getenv('USER')
    if user == "nicholasstroud":
        return Environment.COMPUTER
    else:
        return Environment.RASPBERRY_PI
