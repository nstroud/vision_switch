import cv2
import time
from devices import nanoleaf
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

WINDOW_NAME = "Tracking"
FIXED_SWITCH_LOCATION = True
PLAY_SOUND = False

cam = cv2.VideoCapture(0)
nano_leaf = nanoleaf.NanoLeaf()

time.sleep(1)

timer = cv2.getTickCount()
counter = 0
fps = 0
last_frame_overlap = False
BRIGHTNESS_MAX = 100
current_brightness = 1 * BRIGHTNESS_MAX
nano_leaf.turn_on()
nano_leaf.set_brightness(current_brightness)


def draw_box(image, bbox):
    x, y, width, height = [int(x) for x in bbox]
    cv2.rectangle(image, (x, y), (x + width, y + height), (255, 0, 255), 3, 1)


def get_bounding_box(hand_landmarks, image, normalized=False):
    bounding_box = [1, 1, 0, 0]
    for l in mp_hands.HandLandmark:
        landmark = hand_landmarks.landmark[l]
        if landmark.x < bounding_box[0]:
            bounding_box[0] = landmark.x
        if landmark.y < bounding_box[1]:
            bounding_box[1] = landmark.y
        if landmark.x > bounding_box[2]:
            bounding_box[2] = landmark.x
        if landmark.y > bounding_box[3]:
            bounding_box[3] = landmark.y
    image_height, image_width, _ = image.shape
    if not normalized:
        bounding_box[0] = int(bounding_box[0] * image_width)
        bounding_box[1] = int(bounding_box[1] * image_height)
        bounding_box[2] = int(bounding_box[2] * image_width)
        bounding_box[3] = int(bounding_box[3] * image_height)
    return bounding_box


def convert_bounding_box(box):
    return (box[0], box[1], box[2] - box[0], box[3]-box[1])


def get_brightness_from_box(box):
    y = box[2]
    current_brightness = (1 - y) * BRIGHTNESS_MAX
    if current_brightness < 0:
        return 0
    elif current_brightness > 100:
        return 100
    return int(current_brightness)


with mp_hands.Hands(
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as hands:
    while cam.isOpened():
        _, image = cam.read()
        # Flip the image horizontally for a later selfie-view display, and convert
        # the BGR image to RGB.
        image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)

        # success, bbox = tracker.update(image)
        image.flags.writeable = False
        results = hands.process(image)
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        bounding_box = None
        if results.multi_hand_landmarks:
            # Just take first
            hand_landmarks = results.multi_hand_landmarks[0]
            mp_drawing.draw_landmarks(
                image,
                hand_landmarks,
                mp_hands.HAND_CONNECTIONS,
                mp_drawing_styles.get_default_hand_landmarks_style(),
                mp_drawing_styles.get_default_hand_connections_style())
            bounding_box = get_bounding_box(
                hand_landmarks, image, normalized=True)

        if bounding_box:
            try:
                current_brightness = get_brightness_from_box(bounding_box)
                nano_leaf.set_brightness(current_brightness, 0)
            except Exception as e:
                print(f"Unable to set brightness {e}")
        else:
            cv2.putText(image, 'No hand', (75, 75),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

        if not counter % 7:
            fps = cv2.getTickFrequency()/(cv2.getTickCount()-timer)
        timer = cv2.getTickCount()

        cv2.putText(image, str(int(fps)), (75, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        cv2.putText(image, f"Brightness: {current_brightness}", (75, 70),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 80, 255), 2)

        cv2.imshow(WINDOW_NAME, image)

        if cv2.waitKey(1) & 0xff == ord('q'):
            break
        counter += 1
