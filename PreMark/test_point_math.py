import math
thumb_tip = {
    "x": 0.7162383198738098,
    "y": 0.7077274322509766,
    "z": -0.07099249213933945
}


index_finger_tip = {
    "x": 0.7880433797836304,
    "y": 0.5810379981994629,
    "z": -0.08775452524423599
}

wrist = {
    "x": 0.8290951251983643,
    "y": 0.9213725924491882,
    "z": 0
}


def dist(p, q):
    return math.sqrt(sum((px - qx) ** 2.0 for px, qx in zip(p, q)))


thumb_wrist = dist((thumb_tip["x"], thumb_tip["y"]), (wrist["x"], wrist["y"]))
index_wrist = dist(
    (index_finger_tip["x"], index_finger_tip["y"]), (wrist["x"], wrist["y"]))
index_thumb = dist(
    (index_finger_tip["x"], index_finger_tip["y"]), (thumb_tip["x"], thumb_tip["y"]))
print(thumb_wrist)

angle = math.acos((thumb_wrist**2 + index_wrist**2 -
                   index_thumb**2)/(2*thumb_wrist*index_wrist))
print(angle)
