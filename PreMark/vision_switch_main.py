import cv2
import time
from switch import Switch
import mediapipe as mp
mp_drawing = mp.solutions.drawing_utils
mp_drawing_styles = mp.solutions.drawing_styles
mp_hands = mp.solutions.hands

WINDOW_NAME = "Tracking"
FIXED_SWITCH_LOCATION = True
PLAY_SOUND = False

cam = cv2.VideoCapture(0)
# tracker = cv2.TrackerCSRT_create()  # 3-4
# tracker = cv2.TrackerGOTURN_create()  # failed
# tracker = cv2.TrackerKCF_create()  # 5-6
# tracker = cv2.TrackerMIL_create()  # 5-6
# tracker = cv2.TrackerMOSSE_create()  # 9-16
switch = Switch()

time.sleep(1)

_, image = cam.read()
if FIXED_SWITCH_LOCATION:
    height, width, _ = image.shape
    switch_height = 100
    switch_width = 100
    switch_bbox = (width/2 - switch_width/2, height/2 -
                   switch_height/2, switch_width, switch_height)

else:
    # ID switch box
    switch_bbox = cv2.selectROI(WINDOW_NAME, image, False)
print("switch box")
print(switch_bbox)

# _, image = cam.read()
# bbox = cv2.selectROI(WINDOW_NAME, image, False)
# print("track box")
# print(bbox)
# tracker.init(image, bbox)

timer = cv2.getTickCount()
counter = 0
fps = 0
last_frame_overlap = False


def draw_box(image, bbox):
    x, y, width, height = [int(x) for x in bbox]
    cv2.rectangle(image, (x, y), (x + width, y + height), (255, 0, 255), 3, 1)


def intersection(a, b):
    """a and b given as point and w h"""
    x = max(a[0], b[0])
    y = max(a[1], b[1])
    w = min(a[0]+a[2], b[0]+b[2]) - x
    h = min(a[1]+a[3], b[1]+b[3]) - y
    if w < 0 or h < 0:
        return None
    return x, y, w, h


def get_bounding_box(hand_landmarks, image):
    bounding_box = [1, 1, 0, 0]
    for l in mp_hands.HandLandmark:
        print(l)
        landmark = hand_landmarks.landmark[l]
        print(landmark)
        if landmark.x < bounding_box[0]:
            bounding_box[0] = landmark.x
        if landmark.y < bounding_box[1]:
            bounding_box[1] = landmark.y
        if landmark.x > bounding_box[2]:
            bounding_box[2] = landmark.x
        if landmark.y > bounding_box[3]:
            bounding_box[3] = landmark.y
    image_height, image_width, _ = image.shape
    bounding_box[0] = int(bounding_box[0] * image_width)
    bounding_box[1] = int(bounding_box[1] * image_height)
    bounding_box[2] = int(bounding_box[2] * image_width)
    bounding_box[3] = int(bounding_box[3] * image_height)
    return bounding_box


def convert_bounding_box(box):
    return (box[0], box[1], box[2] - box[0], box[3]-box[1])


with mp_hands.Hands(
        min_detection_confidence=0.5,
        min_tracking_confidence=0.5) as hands:
    while cam.isOpened():
        _, image = cam.read()
        # Flip the image horizontally for a later selfie-view display, and convert
        # the BGR image to RGB.
        image = cv2.cvtColor(cv2.flip(image, 1), cv2.COLOR_BGR2RGB)
        # draw switch
        draw_box(image, switch_bbox)

        # success, bbox = tracker.update(image)
        image.flags.writeable = False

        results = hands.process(image)
        bounding_boxes = []
        image.flags.writeable = True
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        if results.multi_hand_landmarks:
            for hand_landmarks in results.multi_hand_landmarks:
                mp_drawing.draw_landmarks(
                    image,
                    hand_landmarks,
                    mp_hands.HAND_CONNECTIONS,
                    mp_drawing_styles.get_default_hand_landmarks_style(),
                    mp_drawing_styles.get_default_hand_connections_style())
                bounding_box = get_bounding_box(hand_landmarks, image)
                bounding_boxes.append(bounding_box)
                # cv2.rectangle(image, (bounding_box[0], bounding_box[1]), (
                #     bounding_box[2], bounding_box[3]), (0, 255, 0), 2)

        if len(bounding_boxes) > 0:
            overlap = False
            for bounding_box in bounding_boxes:
                bbox = convert_bounding_box(bounding_box)
                # draw object
                draw_box(image, bbox)
                if not overlap:
                    overlap = intersection(switch_bbox, bbox) is not None
            if overlap and overlap is not last_frame_overlap:
                switch.toggle()
                if PLAY_SOUND:
                    switch.play_sound()
            last_frame_overlap = overlap
        else:
            cv2.putText(image, 'lost', (75, 75),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

        if not counter % 7:
            fps = cv2.getTickFrequency()/(cv2.getTickCount()-timer)
        timer = cv2.getTickCount()

        cv2.putText(image, str(int(fps)), (75, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        if switch.is_on():
            cv2.putText(image, "ON", (200, 200),
                        cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 80, 255), 2)
        else:
            cv2.putText(image, "OFF", (200, 200),
                        cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 80, 255), 2)

        cv2.imshow(WINDOW_NAME, image)

        if cv2.waitKey(1) & 0xff == ord('q'):
            break
        counter += 1
