from devices import nanoleaf

leaf = nanoleaf.NanoLeaf()
print(leaf._get_info())

# print(leaf.toggle_power())
print(leaf.set_brightness(100, 1))
print(leaf.list_effects())
print(leaf.get_color_mode())
print(leaf.set_color_blue())
print(leaf.get_color_mode())
