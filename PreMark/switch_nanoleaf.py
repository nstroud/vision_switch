import os
from devices import nanoleaf


class Switch:

    def __init__(self):
        self.on = False
        self.nano_leaf = nanoleaf.NanoLeaf()

    def is_on(self):
        return self.nano_leaf.get_power() is True

    def turn_on(self):
        self.on = True

    def turn_off(self):
        return self.nano_leaf.get_power() is False

    def toggle(self):
        self.on = not self.on
        self.nano_leaf.toggle_power()

    def play_sound(self):
        if self.on:
            os.system('say "On"')
        else:
            os.system('say "off"')

    def set_brightness(self, brightness: int, duration: int = 10):
        if self.is_on():
            return self.nano_leaf.set_brightness(brightness=brightness, duration=duration)
