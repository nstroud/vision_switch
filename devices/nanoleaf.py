from nanoleafapi import Nanoleaf as NanoLeafApi
from nanoleafapi.nanoleaf import BLUE
IP_ADDRESS = "192.168.1.15"
API_BASE = f"http://{IP_ADDRESS}/api/v1"
AUTH_TOKEN = "qn226nvfkNuHccIPqcgTspw0TBdlwMgS"


class NanoLeaf:

    def __init__(self):
        self.device = NanoLeafApi(ip=IP_ADDRESS, auth_token=AUTH_TOKEN)

    def _get_info(self):
        return self.device.get_info()

    def toggle_power(self):
        return self.device.toggle_power()

    def get_power(self):
        return self.device.get_power()

    def turn_on(self):
        return self.device.power_on()

    def turn_off(self):
        return self.device.power_off()

    def set_brightness(self, brightness: int, duration: int = 10):
        return self.device.set_brightness(brightness=brightness, duration=duration)

    def list_effects(self):
        return self.device.list_effects()

    def get_color_mode(self):
        return self.device.get_color_mode()

    def set_color_blue(self):
        return self.device.set_color(BLUE)
